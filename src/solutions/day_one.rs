use std::io;
use solutions::read_to_string;

/// Trait to represent converting a type
/// too `Option<u32>`. If a conversion
/// cannot be made, `None` will be returned.
trait ToU32 {
    fn to_u32(&self) -> Option<u32>;
}

impl ToU32 for char {
    /// Naive implementation for converting `char`
    /// values to u32. For example,
    /// ```
    /// assert_eq!('0'.to_u32().uwrap(), 0);
    /// assert_eq!('0'.to_u32().uwrap(), 5);
    /// assert_eq!('c'.to_u32(), None);
    /// ```
    ///
    fn to_u32(&self) -> Option<u32> {
        let r = match *self {
            '0' => Some(0),
            '1' => Some(1),
            '2' => Some(2),
            '3' => Some(3),
            '4' => Some(4),
            '5' => Some(5),
            '6' => Some(6),
            '7' => Some(7),
            '8' => Some(8),
            '9' => Some(9),
            _ => None,
        };
        r
    }
}

/// Provides the solution to Day 1 Part 1 of Advent of Code 2017
pub fn solution_pt1(filepath: &str) -> Result<u32, io::Error> {
    let (len_data, raw) = read_to_string(filepath)?;
    let first = raw.chars().nth(0).unwrap().to_u32().unwrap();
    let last = raw.chars().nth(len_data - 1).unwrap().to_u32().unwrap();
    let mut acc = match first == last {
        true => first,
        false => 0_u32,
    };
    let mut prev = first;
    for n in raw.chars().map(|ch| ch.to_u32().unwrap()).skip(1) {
        if n == prev {
            acc += n;
        }
        prev = n;
    }
    Ok(acc)
}


/// Provides the solution to Day 1 Part 2 of Advent of Code 2017
pub fn solution_pt2(filepath: &str) -> Result<u32, io::Error> {
    let (len_data, raw) = read_to_string(filepath)?;
    let nums: Vec<_> = raw.chars().map(|ch| ch.to_u32().unwrap()).collect();
    let half = len_data / 2;
    let mut acc = 0_u32;
    let num_iter = len_data - 1;
    for i in 0..num_iter {
        let j = match i > half - 1 {
            true => i - half,
            false => i + half,
        };
        if nums[i] == nums[j] {
            acc += nums[i];
        }
    }
    Ok(acc)
}
