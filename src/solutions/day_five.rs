use std::io;
use solutions::read_to_string;

struct Stack {
    curr: i32,
    steps: i32,
    board: Vec<i32>,
    incr: fn(i32) -> i32,
}

impl Stack {
    fn new(board: Vec<i32>, incr: fn(i32) -> i32) -> Self {
        Stack {
            curr: 0,
            steps: 0,
            board: board,
            incr: incr,
        }
    }

    fn act(&mut self) -> Option<()> {
        let idx = self.curr as usize;
        let val = self.board[idx];
        self.curr += val;
        self.steps += 1;
        if self.curr as usize >= self.board.len() {
            return Some(());
        }
        let incr = self.incr;
        let updated = incr(val);
        self.board[idx] = updated;
        None
    }
}

pub fn solutions(filepath: &str) -> Result<(i32, i32), io::Error> {
    let (_, raw) = read_to_string(filepath)?;
    let nums: Vec<i32> = raw.lines()
        .map(|line| line.parse::<i32>().unwrap())
        .collect();
    let mut s1 = Stack::new(nums.clone(), |n| n + 1);
    let solution1 = loop {
        if let Some(_) = s1.act() {
            break s1.steps;
        }
    };
    let mut s2 = Stack::new(nums, |n| match n >= 3 {
        true => n - 1,
        false => n + 1,
    });
    let solution2 = loop {
        if let Some(_) = s2.act() {
            break s2.steps;
        }
    };
    Ok((solution1, solution2))
}
