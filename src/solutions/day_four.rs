use std::collections::HashSet;
use std::io;
use solutions::read_to_string;

fn is_anagram(w1: &str, w2: &str) -> bool {
    if w1.len() != w2.len() {
        return false;
    }
    let s1: HashSet<char> = w1.chars().collect();
    let s2: HashSet<char> = w2.chars().collect();
    s1 == s2
}

pub fn solution_pt1(filepath: &str) -> Result<u32, io::Error> {
    let (_, raw) = read_to_string(filepath)?;
    let lines = raw.lines();
    let mut acc = 0;
    for line in lines {
        let mut count = 0;
        let mut set: HashSet<&str> = HashSet::new();
        line.split_whitespace()
            .for_each(|val| {
                          let _ = set.insert(val);
                          count += 1;
                      });
        if count == set.len() {
            acc += 1;
        }
    }
    Ok(acc)
}

pub fn solution_pt2(filepath: &str) -> Result<u32, io::Error> {
    let (_, raw) = read_to_string(filepath)?;
    let lines: Vec<&str> = raw.lines().collect();
    let mut acc = 0;
    for line in lines {
        let split: Vec<&str> = line.split_whitespace().collect();
        let len = split.len();
        let mut is_valid = true;
        for i in 0..len {
            for j in i + 1..len {
                if is_anagram(split[i], split[j]) {
                    is_valid = false;
                }
            }
        }
        if is_valid {
            acc += 1;
        }
    }
    Ok(acc)
}
