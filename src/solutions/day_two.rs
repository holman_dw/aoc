use std::io;
use solutions::read_to_string;

fn line_to_nums(line: &str) -> Vec<i32> {
    let parsed: Vec<i32> = line.split_whitespace()
        .map(|s| s.parse::<i32>().unwrap())
        .collect();
    parsed
}

pub fn solution_pt1(filepath: &str) -> Result<i32, io::Error> {
    let (_, raw) = read_to_string(filepath)?;
    let d: i32 = raw.lines()
        .map(|line| {
                 let nums = line_to_nums(line);
                 let (max, min) = (nums.iter().max().unwrap(), nums.iter().min().unwrap());
                 max - min
             })
        .fold(0, |acc, n| acc + n);
    Ok(d)
}

pub fn solution_pt2(filepath: &str) -> Result<i32, io::Error> {
    let (_, raw) = read_to_string(filepath)?;
    let lines: Vec<_> = raw.lines().map(|line| line_to_nums(line)).collect();
    let mut acc = 0;
    for line in lines {
        let len = line.len();
        for i in 0..len {
            for j in i + 1..len {
                let a = line[i];
                let b = line[j];
                if a % b == 0 {
                    acc += a/b;
                } else if b % a == 0 {
                    acc += b/a;
                }
            }
        }
    }
    Ok(acc)
}
