use std::fs::File;
use std::io::prelude::*;
use std::io;
pub mod day_one;
pub mod day_two;
pub mod day_four;
pub mod day_five;
pub mod day_six;

fn read_to_string(filepath: &str) -> Result<(usize, String), io::Error> {
    let mut f = File::open(filepath)?;
    let mut raw = String::new();
    let len = f.read_to_string(&mut raw)?;
    Ok((len, raw))
}
