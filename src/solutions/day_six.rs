use std::io;
use std::collections::HashMap;
use solutions::read_to_string;


struct MemBank {
    blocks: Vec<u32>,
}

impl MemBank {
    fn new(blocks: Vec<u32>) -> Self {
        MemBank {
            blocks
        }
    }

    fn lowest_max(&self) -> usize {
        let mut max_idx = 0 as usize;
        let mut max_val = 0 as u32;
        for (i, &item) in self.blocks.iter().enumerate() {
            if item > max_val {
                max_val = item;
                max_idx = i;
            }
        }
        max_idx
    }

    fn redistribute(&mut self, idx: usize) {
        let val = self.blocks[idx];
        self.blocks[idx] = 0;
        for i in 1..val + 1 {
            let j = get_idx(self.blocks.len(), idx + i as usize);
            self.blocks[j] += 1;
        }
    }
}

fn get_idx(len: usize, idx: usize) -> usize {
    if idx < len {
        return idx
    }
    return get_idx(len, idx - len);
}

pub fn solution_pt1(filepath: &str) -> Result<(usize, usize), io::Error> {
    let (_, raw) = read_to_string(filepath)?;
    let v: Vec<u32> = raw.split_whitespace().map(|n| n.parse::<u32>().unwrap()).collect();
    // let v = vec![0, 2, 7, 0];
    let mut mb = MemBank::new(v.clone());
    let mut count = 0;
    let mut state: HashMap<Vec<u32>, usize> = HashMap::new();
    state.insert(v.clone(), 0);
    loop {
        let i = mb.lowest_max();
        mb.redistribute(i);
        count += 1;
        let _ = match state.get(&mb.blocks) {
            Some(&n) => return Ok((count, count - n)),
            None => ()
        };
        state.insert(mb.blocks.clone(), count);
    }
}