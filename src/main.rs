extern crate aoc;
use aoc::solutions as s;


fn main() {
    let d1_pt1 = match s::day_one::solution_pt1("./data/d1") {
        Ok(n) => n,
        Err(err) => {
            eprintln!("error on day 1: {:?}", err);
            return;
        }
    };
    println!("day one, part one: {}", d1_pt1);
    let d1_pt2 = match s::day_one::solution_pt2("./data/d1") {
        Ok(n) => n,
        Err(err) => {
            eprintln!("error on day 1: {:?}", err);
            return;
        }
    };
    println!("day one, part two: {}", d1_pt2);
    let d2_pt1 = match s::day_two::solution_pt1("./data/d2") {
        Ok(n) => n,
        Err(err) => {
            eprintln!("error on day 2: {:?}", err);
            return;
        }
    };
    println!("day two, part one: {}", d2_pt1);
    let d2_pt2 = match s::day_two::solution_pt2("./data/d2") {
        Ok(n) => n,
        Err(err) => {
            eprintln!("error on day 2: {:?}", err);
            return;
        }
    };
    println!("day two, part one: {}", d2_pt2);
    let d4_pt1 = match s::day_four::solution_pt1("./data/d4") {
        Ok(n) => n,
        Err(err) => {
            eprintln!("error on day 4, {:?}", err);
            return;
        }
    };
    println!("day four, part one: {}", d4_pt1);
    let d4_pt2 = match s::day_four::solution_pt2("./data/d4") {
        Ok(n) => n,
        Err(err) => {
            eprintln!("error on day 4, {:?}", err);
            return;
        }
    };
    println!("day four, part two: {}", d4_pt2);
    let (d5_pt1, d5_pt2) = match s::day_five::solutions("./data/d5") {
        Ok(n) => n,
        Err(err) => {
            eprintln!("error on day 5, {:?}", err);
            return;
        }
    };
    println!("day five, part one: {}", d5_pt1);
    println!("day five, part two: {}", d5_pt2);
    let (d6_pt1, d6_pt2) = match s::day_six::solution_pt1("./data/d6") {
        Ok(n) => n,
        Err(err) => {
            eprintln!("error on day 6, {:?}", err);
            return;
        }
    };
    println!("day six, part one: {}", d6_pt1);
    println!("day six, part two: {}", d6_pt2);
}